<?php

namespace App\Http\Controllers\API\front;

use App\Contracts\FrontAuthContract;
use App\Http\Controllers\BaseController;
use Illuminate\Http\Request;

class AuthController extends BaseController
{
    protected $userRepository;

    public function __construct(FrontAuthContract $userRepository)
    {
        $this->userRepository = $userRepository;
    }

    public function createUser(Request $request)
    {

        $data['user']  = $user = $this->userRepository->createUser($request->all());
        $data['token'] = $user->createToken($user->email.'_token')->plainTextToken;


        $this->responseJson(
            true,
            200,
            "User create successfully",
            $data
        );

        // if (!$user) {
        //     return $this->responseRedirectBack('Error occurred while creating brand.', 'error', true, true);
        // }
        // return $this->responseRedirect('admin.brands.index', 'Brand added successfully', 'success', false, false);

    }
}
